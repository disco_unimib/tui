import {
  Breadcrumbs, Button, Chip, Divider, IconButton, LinearProgress, Stack, Typography, useMediaQuery
} from '@mui/material';
import { MainLayout } from '@components/layout';
import { useAppDispatch, useAppSelector } from '@hooks/store';
import { FC, useEffect, useState } from 'react';
import NavigateNextRoundedIcon from '@mui/icons-material/NavigateNextRounded';
import { Battery, IconButtonTooltip, SplitButton } from '@components/core';
import PlayArrowRoundedIcon from '@mui/icons-material/PlayArrowRounded';
import {
  Link, Redirect, Route, Switch, useHistory, useParams, useRouteMatch
} from 'react-router-dom';
import ArrowBackIosNewRoundedIcon from '@mui/icons-material/ArrowBackIosNewRounded';
import PlayCircleOutlineRoundedIcon from '@mui/icons-material/PlayCircleOutlineRounded';
import SyncRoundedIcon from '@mui/icons-material/SyncRounded';
import {
  annotateDataset, annotateTable,
  getDataset, getDatasetAnnotationStatus,
  getTableAnnotationStatus, getTablesByDataset, uploadDataset, uploadTable
} from '@store/slices/datasets/datasets.thunk';
import {
  selectCurrentDataset,
  selectGetAllDatasetsStatus,
  selectIsUploadDatasetDialogOpen,
  selectIsUploadTableDialogOpen
} from '@store/slices/datasets/datasets.selectors';
import { Status as CompletionStatus } from '@store/slices/datasets/interfaces/datasets';
import AddRoundedIcon from '@mui/icons-material/AddRounded';
import clsx from 'clsx';
import { selectAppConfig } from '@store/slices/config/config.selectors';
import { updateUI } from '@store/slices/datasets/datasets.slice';
import { useSnackbar } from 'notistack';
import SidebarContent from './SidebarContent/SidebarContent';
import ToolbarContent from './ToolbarContent';
import styles from './HomepageChallenge.module.scss';
import Datasets from './Datasets/Datasets';
import Tables from './Tables';
import UploadDialog from './UploadDialogs/UploadDatasetDialog';

export const calcPercentage = (status: CompletionStatus) => {
  const total = Object.keys(status)
    .reduce((acc, key) => status[key as keyof CompletionStatus] + acc, 0);
  const value = (status.DONE / total) * 100;
  return value > 100 ? 100 : value;
};

interface SelectedRowsState {
  kind: 'dataset' | 'table';
  allSelected: boolean;
  rows: any[];
}

const HomepageChallenge: FC<any> = () => {
  const [sidebarCollapsed, setSidebarCollapsed] = useState(false);
  const [selectedRows, setSelectedRows] = useState<SelectedRowsState | null>(null);
  const dispatch = useAppDispatch();
  const {
    path,
    url
  } = useRouteMatch();
  const matches = useMediaQuery('(max-width:1365px)');
  const currentDataset = useAppSelector(selectCurrentDataset);
  const { loading: loadingDatasets } = useAppSelector(selectGetAllDatasetsStatus);
  const { API } = useAppSelector(selectAppConfig);
  const isUploadDatasetDialogOpen = useAppSelector(selectIsUploadDatasetDialogOpen);
  const isUploadTableDialogOpen = useAppSelector(selectIsUploadTableDialogOpen);
  const { enqueueSnackbar } = useSnackbar();

  useEffect(() => {
    dispatch(getDataset({ clearCacheEntry: false }));
  }, []);

  useEffect(() => {
    if (matches) {
      setSidebarCollapsed(true);
    } else {
      setSidebarCollapsed(false);
    }
  }, [matches]);

  const handleSelectedRowsChange = (state: { kind: 'dataset' | 'table', allSelected: boolean, rows: any[] } | null) => {
    setSelectedRows(state);
  };

  const handleCloseDatasetDialog = () => {
    dispatch(updateUI({ uploadDatasetDialogOpen: false }));
  };

  const handleCloseTableDialog = () => {
    dispatch(updateUI({ uploadTableDialogOpen: false }));
  };

  const handleRefresh = () => {
    if (currentDataset) {
      dispatch(getTablesByDataset({ datasetId: currentDataset.id, clearCacheEntry: true }));
    } else {
      dispatch(getDataset({ clearCacheEntry: true }));
    }
  };

  const handleAnnotateDataset = () => {
    if (selectedRows) {
      const { kind, rows } = selectedRows;
      if (kind === 'dataset') {
        // an annotation request for each selected dataset
        rows.forEach((row) => {
          dispatch(annotateDataset({
            datasetId: row.id
          }))
            .unwrap()
            .then(() => {
              enqueueSnackbar('Annotation queued successfully.', {
                variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'center'
                }
              });
              dispatch(getDatasetAnnotationStatus({ datasetId: row.id }));
            })
            .catch((err) => {
              enqueueSnackbar(err, {
                variant: 'error',
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'center'
                }
              });
            });
        });
      }
    }
  };

  const handleAnnotateTables = () => {
    if (selectedRows) {
      const { kind, allSelected, rows } = selectedRows;
      if (kind === 'table') {
        // if all tables are selected, request to annotate dataset
        if (allSelected) {
          dispatch(annotateDataset({
            datasetId: currentDataset.id
          })).unwrap().then(() => {
            enqueueSnackbar('Annotation queued successfully.', {
              variant: 'success',
              anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'center'
              }
            });
            dispatch(getDatasetAnnotationStatus({ datasetId: currentDataset.id }));
          }).catch((err) => {
            enqueueSnackbar(err, {
              variant: 'error',
              anchorOrigin: {
                vertical: 'bottom',
                horizontal: 'center'
              }
            });
          });
        } else {
          // a request for each selectedTable
          rows.forEach((row) => {
            dispatch(annotateTable({
              datasetId: currentDataset.id,
              tableId: row.id
            })).unwrap().then(() => {
              enqueueSnackbar('Annotation queued successfully.', {
                variant: 'success',
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'center'
                }
              });
              dispatch(getTableAnnotationStatus({
                datasetId: currentDataset.id,
                tableId: row.id
              }));
            }).catch((err) => {
              enqueueSnackbar(err, {
                variant: 'error',
                anchorOrigin: {
                  vertical: 'bottom',
                  horizontal: 'center'
                }
              });
            });
          });
        }
      }
    }
  };

  const onSubmitNewDataset = (formValues: Record<string, any>) => {
    const formData = new FormData();
    Object.keys(formValues).forEach((key) => {
      formData.append(key, formValues[key]);
    });
    dispatch(uploadDataset({ formData }))
      .unwrap()
      .then(() => {
        dispatch(updateUI({ uploadDatasetDialogOpen: false }));
      });
  };

  const onSubmitNewTables = (formValues: Record<string, any>) => {
    const formData = new FormData();
    Object.keys(formValues).forEach((key) => {
      formData.append(key, formValues[key]);
    });
    dispatch(uploadTable({ formData, datasetId: currentDataset.id }))
      .unwrap()
      .then(() => {
        dispatch(updateUI({ uploadTableDialogOpen: false }));
      });
  };

  const startProcess = (option: string) => {
    const endpoint = API.ENDPOINTS.PROCESS_START.find((value) => value.name === option);
    if (endpoint) {
      if (selectedRows) {
        const { kind, allSelected, rows } = selectedRows;
        if (kind === 'dataset') {
          // an annotation request for each selected dataset
          rows.forEach((row) => {
            dispatch(annotateDataset({
              datasetId: row.id
            }));
          });
        } else {
          if (allSelected) {
            dispatch(annotateDataset({
              datasetId: currentDataset.id
            }));
          } else {
            rows.forEach((row) => {
              dispatch(annotateTable({
                datasetId: currentDataset.id,
                tableId: row.id
              }));
            });
          }
        }
      }
    }
  };

  let breadcrumbsDatasetProps = {};
  if (currentDataset) {
    breadcrumbsDatasetProps = {
      component: Link,
      to: '/datasets',
      className: clsx([styles.BreadcrumbsItem, styles.BreadcrumbsLink])
    };
  }

  return (
    <MainLayout
      ToolbarContent={<ToolbarContent />}
      sidebarCollapsed={sidebarCollapsed}
      sibebarCollapseChange={() => setSidebarCollapsed((old) => !old)}>
      <div className={styles.Header}>
        <div className={styles.Column}>
          <div className={clsx(
            styles.Row
          )}>
            <Breadcrumbs separator={<NavigateNextRoundedIcon fontSize="small" />}>
              <Typography
                className={styles.BreadcrumbsItem}
                {...breadcrumbsDatasetProps}
                variant="h6">
                Datasets
              </Typography>
              {currentDataset && (
                <Typography
                  className={styles.BreadcrumbsItem}
                  variant="h6">
                  {currentDataset.name}
                </Typography>
              )}
            </Breadcrumbs>
          </div>
          <div className={clsx(styles.Row, styles.SubHeader)}>
            {selectedRows && (
              <div className={styles.NSelected}>
                <strong>{selectedRows.rows.length}</strong>
                &nbsp;selected
              </div>
            )}
            <Stack direction="row" gap="5px">
              {currentDataset ? (
                <>
                  {API.ENDPOINTS.ANNOTATE_TABLE && (
                    <Button
                      sx={{
                        textTransform: 'none'
                      }}
                      onClick={handleAnnotateTables}
                      variant="contained"
                      endIcon={<PlayCircleOutlineRoundedIcon />}
                      disabled={!selectedRows || selectedRows.rows.length === 0}>
                      Annotate tables
                    </Button>
                  )}

                </>
              ) : (
                <>
                  {API.ENDPOINTS.ANNOTATE_DATASET && (
                    <Button
                      sx={{
                        textTransform: 'none'
                      }}
                      onClick={handleAnnotateDataset}
                      variant="contained"
                      endIcon={<PlayCircleOutlineRoundedIcon />}
                      disabled={!selectedRows || selectedRows.rows.length === 0}>
                      Annotate dataset
                    </Button>
                  )}
                </>
              )}
              <IconButtonTooltip
                onClick={handleRefresh}
                Icon={SyncRoundedIcon}
                tooltipText="Refresh data" />
            </Stack>
            {/* {API.ENDPOINTS.PROCESS_START && API.ENDPOINTS.PROCESS_START.length > 0 && (
              <SplitButton
                prefix="Start process:"
                handleClick={startProcess}
                disabled={!selectedRows || selectedRows.rows.length === 0}
                options={API.ENDPOINTS.PROCESS_START.map(({ name }) => name)} />
            )} */}
            {/* {API.ENDPOINTS.PROCESS_START && API.ENDPOINTS.PROCESS_START.length > 0 && (
              <SplitButton
                prefix="Start process:"
                handleClick={startProcess}
                disabled={!selectedRows || selectedRows.rows.length === 0}
                options={API.ENDPOINTS.PROCESS_START.map(({ name }) => name)} />
            )} */}
            {/* <Button
              className={styles.ButtonAnnotation}
              disabled={!selectedRows || selectedRows.rows.length === 0}
              variant="contained"
              size="small"
              endIcon={<PlayArrowRoundedIcon />}>
              Start annotation
            </Button> */}
          </div>
        </div>
        <Stack direction="row" gap="20px" alignItems="center">
          {API.ENDPOINTS.UPLOAD_DATASET
            && (
              <Button
                size="small"
                component="label"
                startIcon={<AddRoundedIcon />}
                color="primary"
                onClick={() => {
                  if (currentDataset) {
                    dispatch(updateUI({ uploadTableDialogOpen: true }));
                  } else {
                    dispatch(updateUI({ uploadDatasetDialogOpen: true }));
                  }
                }}
                variant="text">
                  {currentDataset ? 'New Table' : 'New Dataset'}
              </Button>
            )
          }
          {currentDataset && (
            <Stack
              alignItems="center"
              direction="row"
              gap="10px"
              divider={<Divider orientation="vertical" flexItem />}
            >
              <Stack justifyContent="center" direction="column" gap="10px">
                <Chip label={`Tables: ${currentDataset.nTables}`} size="small" />
                <Chip label={`Avg cols: ${currentDataset.nAvgCols}`} size="small" />
                <Chip label={`Avg rows: ${currentDataset.nAvgRows}`} size="small" />
              </Stack>

              <Stack gap="10px" alignItems="center">
                <Typography variant="body2" fontWeight="500">Dataset completion</Typography>
                <Battery size="medium" value={calcPercentage(currentDataset.status)} />
              </Stack>
            </Stack>
          )
          }
        </Stack>
      </div>
      <div className={styles.TableContainer}>
        <Switch>
          <Route exact path={path}>
            <Datasets onSelectionChange={handleSelectedRowsChange} />
          </Route>
          <Route path={`${path}/:datasetId/tables`}>
            {loadingDatasets === false
              ? <Tables onSelectionChange={handleSelectedRowsChange} /> : <LinearProgress />}
          </Route>
          <Redirect from="*" to="/datasets" />
        </Switch>
      </div>
      <UploadDialog
        title="Upload dataset"
        context="Select a dataset to upload (.zip). Each dataset should contain one or more tables and should have a size limit of 500MB."
        loading={false}
        open={isUploadDatasetDialogOpen}
        onClose={handleCloseDatasetDialog}
        handleCancel={handleCloseDatasetDialog}
        nameLabel="Dataset name"
        nameValue="datasetName"
        onSubmit={onSubmitNewDataset}
      />
      {currentDataset && (
        <UploadDialog
          title="Upload table"
          context="Select tables to upload (.zip). Tables should be grouped in a zip file and the file should have a size limit of 500MB."
          loading={false}
          defaultNameValue={currentDataset.name}
          open={isUploadTableDialogOpen}
          onClose={handleCloseTableDialog}
          handleCancel={handleCloseTableDialog}
          nameLabel="Dataset name"
          nameValue="datasetName"
          onSubmit={onSubmitNewTables}
        />
      )}
      {/* <UploadDataset /> */}
    </MainLayout>
  );
};

export default HomepageChallenge;
