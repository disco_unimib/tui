import {
  Dialog, DialogTitle, DialogContent,
  DialogContentText, DialogProps
} from '@mui/material';
import { FC } from 'react';
import UploadForm, { UploadFormProps } from '../Upload/Upload';

export type UploadDatasetDialogProps = DialogProps & UploadFormProps & {
  title: string;
  context: string;
};

const UploadDialog: FC<UploadDatasetDialogProps> = ({
  title,
  context,
  nameLabel,
  nameValue,
  defaultNameValue,
  loading,
  handleCancel,
  onSubmit,
  ...props
}) => {
  const uploadFormProps = {
    nameLabel,
    nameValue,
    defaultNameValue,
    loading,
    handleCancel,
    onSubmit
  };

  return (
    <Dialog {...props}>
      <DialogTitle>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>
          {context}
        </DialogContentText>
        <UploadForm {...uploadFormProps} />
      </DialogContent>
    </Dialog>
  );
};

export default UploadDialog;
