import {
  Button, Stack,
  TextField, Typography
} from '@mui/material';
import {
  ChangeEvent, FC, forwardRef,
  HTMLAttributes, useState
} from 'react';
import { LoadingButton } from '@mui/lab';
import { Controller, useForm } from 'react-hook-form';

// export type UploadProps = {
//   loading: boolean;
//   errors: Record<string, string>;
//   handleConfirm: (formData: FormData) => void;
// }

export type UploadProps = HTMLAttributes<HTMLInputElement> & {
  onChange: (file: File) => void;
  error: boolean;
  helperText: any;
  supportedFormats?: string[];
};

const DEFALT_FORMATS = ['zip'];
const DEFAULT_SIZE = 500;

const toMB = (bytes: number) => bytes / (1024 * 1024);

const UploadHandler = forwardRef<HTMLInputElement, UploadProps>(({
  supportedFormats = DEFALT_FORMATS,
  helperText,
  error,
  onChange,
  ...props
}, ref) => {
  const [state, setState] = useState<File>();

  const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
    setState(undefined);
    const { files } = event.target;
    if (files && files.length === 1) {
      const file = files[0];
      setState(file);
      if (onChange) {
        onChange(file);
      }
    }
  };

  return (
    <Stack gap="10px">
      <Stack direction="row" alignItems="center" gap="10px">
        <Button
          sx={{
            alignSelf: 'flex-start',
            textTransform: 'none'
          }}
          size="medium"
          component="label"
          color="primary"
          variant="contained">
          Select file (.zip)
          <input
            ref={ref}
            type="file"
            onChange={handleFileChange}
            hidden
            {...props}
          />
        </Button>
        {state && <Typography>{state.name}</Typography>}
      </Stack>
      {error && (
        <Typography fontSize="12px" margin="3px 14px 0" color="error">{helperText}</Typography>
      )}
    </Stack>
  );
});

export type UploadFormProps = {
  nameLabel: string;
  nameValue: string;
  defaultNameValue?: string;
  loading: boolean;
  handleCancel: () => void;
  onSubmit: (formValues: Record<string, any>) => void;
}

const UploadForm: FC<UploadFormProps> = ({
  nameLabel,
  nameValue,
  defaultNameValue = '',
  loading,
  handleCancel,
  onSubmit
}) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    control
  } = useForm({
    defaultValues: {
      [nameValue]: defaultNameValue,
      file: ''
    }
  });

  return (
    <Stack component="form" padding="10px 0" gap="10px" onSubmit={handleSubmit(onSubmit)}>
      <TextField
        error={!!errors[nameValue]}
        helperText={errors[nameValue] ? errors[nameValue].message : ''}
        label={nameLabel}
        variant="outlined"
        disabled={!!defaultNameValue}
        {...register(nameValue, {
          required: {
            value: true,
            message: 'This field is required'
          }
        })} />
      <Controller
        render={({ field: { onChange } }) => (
          <UploadHandler
            error={!!errors[nameValue]}
            helperText={errors.file ? errors.file.message : ''}
            onChange={onChange} />
        )}
        rules={{
          validate: {
            required: (value: any) => !!value || 'This field is required',
            size: (value: any) => toMB(value.size) <= DEFAULT_SIZE || 'File size should not be over 500MB',
            format: (value: any) => {
              return (value.type === 'application/x-zip-compressed' || value.type === 'application/zip') || 'File should be a .zip';
            }
          }
        }}
        control={control} 
        name="file"
      />
      <Stack direction="row" justifyContent="flex-end">
        <Button onClick={handleCancel}>
          Cancel
        </Button>
        <LoadingButton type="submit" loading={loading}>
          Confirm
        </LoadingButton>
      </Stack>
    </Stack>
  );
};

export default UploadForm;
