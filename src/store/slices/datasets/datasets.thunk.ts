import { createAsyncThunk } from '@reduxjs/toolkit';
import datasetAPI from '@services/api/datasets';
import { ID } from '@store/interfaces/store';
import { setCurrentDataset } from './datasets.slice';

const ACTION_PREFIX = 'dataset';

export enum DatasetThunkActions {
  GET_DATASET = 'getDataset',
  GET_DATASET_INFO = 'getDatasetInfo',
  GET_TABLES_BY_DATASET = 'getTablesByDataset',
  GET_DATASET_ANNOTATION_STATUS = 'getDatasetAnnotationStatus',
  GET_TABLE_ANNOTATION_STATUS = 'getTableAnnotationStatus',
  ANNOTATE_DATASET = 'annotateDataset',
  ANNOTATE_TABLE = 'annotateTable',
  GLOBAL_SEARCH = 'globalSearch',
  UPLOAD_DATASET = 'uploadDataset',
  UPLOAD_TABLE = 'uploadTable'
}

export const getDataset = createAsyncThunk(
  `${ACTION_PREFIX}/${DatasetThunkActions.GET_DATASET}`,
  async ({ clearCacheEntry = false }: { clearCacheEntry: boolean }) => {
    const response = await datasetAPI.getDataset({}, clearCacheEntry);
    return response.data;
  }
);

export const getDatasetInfo = createAsyncThunk(
  `${ACTION_PREFIX}/${DatasetThunkActions.GET_DATASET_INFO}`,
  async ({ datasetId }: { datasetId: ID }) => {
    const response = await datasetAPI.getDatasetInfo({ datasetId });
    return response.data;
  }
);

export const getTablesByDataset = createAsyncThunk(
  `${ACTION_PREFIX}/${DatasetThunkActions.GET_TABLES_BY_DATASET}`,
  async ({
    datasetId, clearCacheEntry
  }: { datasetId: ID, clearCacheEntry: boolean }, { dispatch }) => {
    dispatch(setCurrentDataset(datasetId));
    const response = await datasetAPI.getTablesByDataset({ datasetId }, clearCacheEntry);
    return {
      data: response.data,
      datasetId
    };
  }
);

export type AnnotateDatasetThunkRequestProps = {
  datasetId: string;
}

export const annotateDataset = createAsyncThunk<{}, AnnotateDatasetThunkRequestProps>(
  `${ACTION_PREFIX}/${DatasetThunkActions.ANNOTATE_DATASET}`,
  async ({ datasetId }) => {
    const response = await datasetAPI.annotateDataset(datasetId);
    return {
      data: response.data
    };
  }
);

export type AnnotateTableThunkRequestProps = {
  datasetId: string;
  tableId: string;
}

export const annotateTable = createAsyncThunk<{}, AnnotateTableThunkRequestProps>(
  `${ACTION_PREFIX}/${DatasetThunkActions.ANNOTATE_TABLE}`,
  async ({ datasetId, tableId }) => {
    const response = await datasetAPI.annotateTable(datasetId, tableId);
    return {
      data: response.data
    };
  }
);

export type GetDatasetAnnotationStatusProps = {
  datasetId: string;
}

export const getDatasetAnnotationStatus = createAsyncThunk<
Record<string, any>, GetDatasetAnnotationStatusProps
>(
  `${ACTION_PREFIX}/${DatasetThunkActions.GET_DATASET_ANNOTATION_STATUS}`,
  async (params) => {
    const response = await datasetAPI.getDatasetAnnotationStatus(params);
    return {
      data: response.data,
      datasetId: params.datasetId
    };
  }
);

export type GetTableAnnotationStatusProps = {
  datasetId: string;
  tableId: string;
}

export const getTableAnnotationStatus = createAsyncThunk<
Record<string, any>, GetTableAnnotationStatusProps
>(
  `${ACTION_PREFIX}/${DatasetThunkActions.GET_TABLE_ANNOTATION_STATUS}`,
  async (params) => {
    const response = await datasetAPI.getTableAnnotationStatus(params);
    return {
      data: response.data,
      datasetId: params.datasetId,
      tableId: params.tableId
    };
  }
);

export const globalSearch = createAsyncThunk(
  `${ACTION_PREFIX}/${DatasetThunkActions.GLOBAL_SEARCH}`,
  async ({ query }: { query: string }) => {
    const response = await datasetAPI.globalSearch(query);
    return response.data;
  }
);

export const uploadDataset = createAsyncThunk(
  `${ACTION_PREFIX}/${DatasetThunkActions.UPLOAD_DATASET}`,
  async ({ formData }: { formData: FormData }) => {
    const response = await datasetAPI.uploadDataset(formData);
    return response.data;
  }
);

export const uploadTable = createAsyncThunk(
  `${ACTION_PREFIX}/${DatasetThunkActions.UPLOAD_TABLE}`,
  async ({ formData, datasetId }: { formData: FormData, datasetId: string }) => {
    const response = await datasetAPI.uploadTable(formData, datasetId);
    return response.data;
  }
);
