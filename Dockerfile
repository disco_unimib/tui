FROM node:12.16.1-alpine3.9 as build
WORKDIR /app
COPY package*.json /app/
RUN npm install
COPY ./ /app
RUN DISABLE_ESLINT_PLUGIN=true npm run build

# stage 2 - build the final image and copy the react build files
FROM nginx:1.17.8-alpine
COPY --from=build /app/build /usr/share/nginx/html
COPY --from=build /app/nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]