# tUI - Table UI

tUI is an open-source tool which provides ways to both annotate a raw table and easily view data of semantically annotated tables. It allows to easily inspect cell annotations, column types and column relations.
tUI is built using React and it's easily accessible from every browser.


![Datasets](/images/datasets.png)
![Tables](/images/tables.png)
![Column relations](/images/relations.png)
![Metadata selection](/images/metadata.png)
![Expand cells](/images/expand.png)

## Getting Started

The following instructions will guide you to setup the application on your local machine.

### Prerequisites
The application requires `nodeJS and NPM`, so you first need to install those to run it. You can follow this guide to do so https://docs.npmjs.com/downloading-and-installing-node-js-and-npm.

### Steps:

1. Clone the repository and install the requirements:
```
npm install
```
2. The application doesn't come with API endpoints preconfigured so that each user can provide their own APIs. We provide a `config.yaml` to facilitate the setup. You can edit the configuration file for your needs (see [config.yaml](https://bitbucket.org/disco_unimib/tui/src/master/src/config.yaml)).
3. Run the application:
```
npm run start
```

### config.yaml
The config.yaml provides a way to easily customize API endpoints.

* `${ANY_STRING}`: identifies an environment variabile. You can store sensible information in an env file (a `.env-sample` file is made available for you). This is useful to specify server endpoints.

* API responses / requests have to follow a specific schema. Examples are reported in `examples_responses_requests`.

